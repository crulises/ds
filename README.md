# Acerca

Este repositorio surge como una reestructuración para llevar el seguimiento a
un proyecto de dinámica de sistemas.

El propósito general es contar con un lugar en donde gestionar toda la
información recaudada y poder consultar lo que sea necesario.
