# Variables auxiliares

1. Plazo restante: representa el tiempo disponible que tiene el alumno para
   finalizar las tareas que tenga pendientes.
   - Presión de fecha de entrega: La cantidad de tiempo que se disponga en
     `Plazo restante` afectará la presión que el alumno tenga para finalizar
     el proyecto.
2. Cantidad de prorrogas disponibles: establece la cantidad de veces que se le
   puede brindar prorrogas a un alumno determinado antes de determinar que el
   mismo debe empezar otro proyecto nuevo.
   - Prorroga: se ajusta el computo de prorroga con respecto a cuantas
     prorrogas se tengan disponibles.
3. Prorroga normal: valor que indica la cantidad de días que se prolonga la
   presentación del trabajo en días, esta se le dará al alumno si el alcanza el
   `Porcentaje aceptable de avance del proyecto`
   - Prorroga
4. Porcentaje aceptable de avance del proyecto: este determina en que porcentaje
   debe estar el proyecto para que la cátedra defina si se le puede brindar una
   prorroga o no al alumno.
   - Prorroga: acá la el porcentaje va a ser uno de los factores críticos para
     determinar si la prorroga se da o no.
5. Prorroga: este representa al tiempo extra que se le da al alumno para que
   pueda presentar un proyecto con un porcentaje de avance mayor y/o
   finalizado.
   - Plazo restante: el tiempo que se tenga en la prorroga va a extender el
     plazo restante.
6. Presión de fecha de entrega: este es otra forma de plantear la productividad
   de un alumno en un tiempo determinado.
   - Trabajo (t: Flujo): la presión de la fecha de entrega va a tener
     consecuencias con la cantidad de trabajo del alumno, cuando esta presión
     sea grande, el alumno va a trabajar más (va a ser mas productivo),
     cuando la presión sea menor el alumno va ser mas ocioso (menos
     productivo)
7. Porcentaje de Avance: este es un calculo que relaciona la cantidad de
   `Tareas pendientes` y las `Tareas finalizadas` para obtener un porcentaje de
   avance del proyecto
   - Prorroga: el valor del porcentaje de avance es uno de los factores que va
     a determinar si el estudiante puede obtener o no una prorroga
   - Presión de fecha de entrega: el valor del porcentaje de avance va a hacer
     que el alumno varíe su nivel de productividad en donde va a disminuir
     cuando el `Porcentaje de avance` es mayor y aumentar cuando el
     `Porcentaje de avance` es menor (Básicamente, el alumno se duerme en los
     laureles)
8. Cantidad de tareas iniciales (iffi): pretende ser un valor simple que
   indique con cuantas tareas se inicia el proyecto.
   > A esto se lo puede mejorar adaptando una variable de nivel que se
   > modifique en base a las charlas pre-aprobatorias de los diferentes proyectos
   > que se ven en la cátedra
   - Tareas Pendientes (t: V. Nivel): el valor de `Cantidad de tareas iniciales`
     será de ayuda a la hora de iniciar la simulación ya que con esta tenemos
     un valor que nos dice la cantidad de tareas que se tienen pendientes.
9. Acceso a material de ayuda: el valor de esta variable será un indicador que
   determine que tan rápido el alumno puede ser productivo. Si este no cuenta
   con acceso a `Material de ayuda` entonces invertirá más tiempo realizando
   correcciones ya que el trabajo puede estar equivocado entonces se suman
   tareas debido a la `Generación de errores`
   - Trabajo (t: Flujo): el material de ayuda será un factor que incrementará
     la productividad del alumno.
10. Carga horaria de dedicación al proyecto: este valor determina cuanto tiempo
    el alumno le dedica exclusivamente al proyecto.
    - Trabajo (t: Flujo): el valor que posea
      `Carga horaria de dedicación al proyecto` será un factor que influirá de
      manera positiva en la productividad del alumno.
11. Tiene familia: este pretende ser un componente que brinde fricción a la
    hora de la carga horaria que se le dedique a la facultad y al proyecto.
    - Carga horaria de dedicación al proyecto: en el modelo se pretende que la
      variable familia (hijos, cónyuge) afecte de manera negativa a la
      productividad del alumno.
12. Trabaja: esta es otra variable que busca introducir fricción a la hora de
    computar la carga horaria que se le dedica a la facultad y al proyecto.
    - Carga horaria de dedicación al proyecto: aquí, el valor de `Trabaja`
      impactará de manera positiva o negativa a la productividad. ¿Por qué?
      Hubieron trabajos en los cuales los alumnos realizaban el proyecto bajo
      el paraguas de la empresa en la que trabajaban (esto es un panorama no
      muy rebuscado, ya que a estas alturas los mismos ya cuentan con
      conocimientos que les va a permitir contribuir a un equipo de desarrollo).
13. Carga horaria dedicada a la facultad: esta variable busca ser un factor que
    ayuda a la dedicación horaria al proyecto, esta se consigue mediante la
    combinación de otras variables.
    - Carga horaria de dedicación al proyecto: cuanto mas carga horaria se
      dedique a la facultad, más carga horaria será dedicada al proyecto
14. Carga horaria de consulta: representa la carga horaria que el alumno
    invierte en los horarios extracurriculares de consulta a los docentes.
    - Carga horaria dedicada a la facultad: busca ser un amplificador para la
      carga horaria que se dedica a la facultad.
15. Cantidad de materias cursando: si bien esta aumenta la dedicación, del
    alumno hacia la facultad, la cantidad de materias también afectará la
    cantidad de horas dedicadas exclusivamente al proyecto
    - Carga horaria dedicada a la facultad: esta se ve amplificada porque se
      aumenta la dedicación de acuerdo a la cantidad de materias que curse el
      alumno. Sin embargo, cuanto mas materias curse también tendrá menos
      tiempo que dedicar al proyecto

> Notas: estas variables son bastante importantes, acá puede ser necesario mas
> diseño, ya que la experiencia va a verse afectada por errores y correcciones de
> cátedra como también por las tareas finalizadas exitosamente

16. Experiencia: esta busca introducir amplificación pero también puede
    significar introducir fricción. Cuando un alumno con experiencia en
    desarrollo inicia el proyecto (ej: ya se encuentra en un entorno laboral, por
    lo que su experiencia es alta), este tendrá menos dificultades técnicas.

    - Trabajo (t: Flujo): esta variable incrementa la productividad del alumno,
      cuanto más experiencia tenga el mismo con el desarrollo, más productivo
      será, además este introducirá menos errores.

17. Bases de datos: esta busca cuantificar cuanta experiencia tiene un alumno a
    la en el diseño, implementación y manipulación de las bases de datos. Esto
    será esencial a la hora de trabajar con un sistema transaccional, y diseñar
    una base de datos de manera correcta/incorrecta puede afectar en gran
    medida al desempeño del alumno.
    - Experiencia: la experiencia en base de datos es uno de los factores que
      amplifica/disminuye el valor total que se tiene de experiencia.
18. Análisis de Sistemas: esta busca determinar que tanta experiencia tiene el
    alumno con el diseño de sistemas en general (Identificar los requisitos del usuario,
    identificar los módulos necesarios, identificar qué actores son necesarios
    y cuales son las operaciones que cada uno puede realizar,
    diseño de entradas, diseño de salidas, etc)
    - Experiencia: la experiencia en análisis de sistemas es otro de los factores que
      amplifica/disminuye el valor total que se tiene de experiencia.
19. Lenguaje: este busca cuantificar el manejo del alumno en el stack
    seleccionado para llevar a cabo el proyecto. Dentro de la cátedra esta es
    una decisión que el alumno realiza por sí mismo, se tienen experiencias en
    donde esta decisión ha llevado al fracaso a proyectos.
    - Experiencia: la experiencia en el lenguaje (stack) es otro de los factores que
      amplifica/disminuye el valor total que se tiene de experiencia.

## Variables auxiliares desconectadas

20. Enfermedad
    - Cantidad horaria dedicada al proyecto
21. Factor de disminución de productividad por enfermedad
    - Enfermedad

# Variables de nivel

22. Errores (Flujo: Errores transformados a tareas pendientes) Tareas pendientes
23. Tareas pendientes (Flujo: Trabajo) Tareas finalizadas
    - Porcentaje de avance
24. Tareas Finalizadas
    - Porcentaje de avance
    - Experiencia
25. Correcciones de cátedra (Flujo: Correcciones de cátedra pasadas a tareas
    pendientes) Tareas pendientes

# Flujos

26. Generación de errores
27. Errores transformados a tareas pendientes
28. Revisión de cátedra
29. Correcciones de cátedra pasadas a tareas pendientes
30. Trabajo
